# BlockGrade

BlockGrade is a blockchain-based system for creating, verifing and viewing courses grades and certificates. Built upon Ethereum blockchain.
The project is still a proof of concept.


##**Setting up environment**:

###Before you start, make sure to have these available:

* [Node.js and npm](https://nodejs.org/en/), npm is a package manager that you need to run the dApp.
* A Unix shell, if you're a Windows user you'll need to install [Git bash](https://git-scm.com/).


###Once you have requirements installed:

1. Install Truffle framework with the following
   command using Git bash or your favourite terminal 
   `npm install -g truffle`,
   Truffle is a Smart Contracts development environment that you need to run this project.
   Run `truffle version` to check that Truffle is installed correctly, it should output no errors.

2. Then you need to install [Ganache](https://truffleframework.com/ganache), a personal blockchain for
   Ethereum that you will use to deploy contracts and run tests.

3. Last but not least, install [MetaMask](https://metamask.io/), a browser extension for both Chrome
   and Firefox to interact with our dApp.


###Configuring Ganache and MetaMask:

You need to configure Ganache and MetaMask before you can start interacting with the dApp.

1. Open the fox icon in your browser bar and start Ganache.

2. At the intial MetaMask screen. Click **Import Existing DEN.**

3. In the box marked **Wallet Seed**, enter the mnemonic that is displayed in Ganache.

4. Now you need to connect MetaMask to the blockchain created by Ganache. Click the menu that shows "Main Network" and select **Custom RPC**.

5. In the box titled "New RPC URL" enter the RPC server displayed in Ganache and click **Save**. 


###Running the dApp:

1. Clone the repository.

2. Change directory to the project.
   `cd blockgrade`

3. Run `npm install`, in the terminal.

4. Make sure Ganache and MetaMask are configured.

5. Run `truffle compile`, then `truffle migrate` in the terminal.

6. Run `npm run dev` in the terminal to start the local web server.


##**Pages**:

### Starting Page

This web app starts in a Start Page where you create your own substitution.


* you have **Create Institution** where you enter the public key of the head and the name of the institution followed by the public keys of the creator and at least two board members. The institution address will be shown on your screen.


* Here you have another section int this page **Go to Institution** using the address shown on the screen to go to the Institution Page.

###Institution Page

In this page you have multiple features.

* **Add sub-Institution** option, By entering the head public key and the institution name followed by at least two board members. This option forms the hierarchy needed to build your tree for example you build an institution for Cairo University then you build your sub institutions (Faculties). 


* If you want to **Add Course** to the institution you created. By entering the course name, the instructor public key and at least two teaching assistants. After you **submit** that process the created course address will be out on window, with that address you will be able to go to course page and use its features. 


* **Add board member** feature and **Remove board member** that takes the public key of the added or removed member in this institution.


* **Change Head** feature that takes the public key of the selected new head.


* **Voting** feature it is for all the options selected by the user (add member / remove member / Change head ), this feature takes the votes from the current members in the institution and if more than half percent of the current members voted for an option it is approved by the smart contract.

### Course Page 

In this page you can interact with the course contract.


* First we have **Parent Institution** button that redirects you to the parent institution that created the course.


* **Start Date** and **End Date** show start and end dates respectively of the course.


>Note: At the beginning, Start and date dates are set to zero.


*  **Course Address** shows the contract's address


>** Before using any of the Course functions declare both End and Start Dates as they are required for all the contract functions.**


*  **Add Instructor** and **Add Teaching Assistants** register instructors and TAs  respectively to the course,  **Remove instructor** and **Remove Teaching Assistant** removes them. 


> Note: Only registered instructors in the course can add and remove instructors or add and remove TAs.


* **Add Student** and **Remove Student** register and remove students respectively


> Note: Only registered instructors and TAs can add or remove students.


* From the **course duration** section we can edit **Start Date** and **End Date** of the course.


> Note: Start date should be yet to come and of course End date should by greater than Start date, plus Start date cannot be edited once the course starts.


*  From the **Add Graded Item** we can add graded items to the course which represents assignments, quizzes, exams, etc... . The **Weight** field represents how much does this item contributes to the course total grade. You cannot add new items before course's start date and cannot add new ones after course's end date.


> Note: Course's total weight is 100, so you cannot assign items with a total weight of more than 100, but can set the maximum grade an item as much as you like. Registered instructors only can add graded items.


* **Update Graded Item Weight** Can be used to change the weight of an existing item using the id of this item and the new weight to be assigned if possible. Graded items ids are serial and begins at zero so the first graded item has the id of 0 and so on. You cannot update graded items after course's end date.


> Note: Registered instructors only can update graded items weight.


* **Update Student Grade** assign a graded item grade for a student by entering the student's PK, grade to be assigned, graded item id and a description for this grading update.


> Note: Only registered instructors or TAs can assign grades to registered students.


> If a graded item graded is updated after course's end date for whatever reason. The student's total grade is recalculated automatically. 


* After the course ends the student can calculate his final grade from **Compute Student Grade** section by entering his PK.

### Student Page

This page allows the student to check his grades in details of any course that he is applied to.

* this feature **Get Grades** shows the grades of the course after taking the public key of both the student and the required course.