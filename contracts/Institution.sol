pragma solidity 0.5.0;

import "./Course.sol";
//definitely not the best way to implement this features, so provide your feedback and contribution please.

//seems like solidity does not allow you to create a contract instance from the same type "Circular reference"
contract InstitutionFactory{

    event CreateInstitution(string msg , address _address);
    function createInstitution(string memory _name , address head, address creator , address[] memory _boardMembers) public returns(address){
        Institution subInstitute = new Institution(_name , head , creator , _boardMembers);
        emit CreateInstitution("Created Institution" , address(subInstitute));
        return address(subInstitute);
    }
}


/// @title Institution
contract Institution{

    address private creator; //The address of parent institution. Has no privileges over the contract, just for backtracking purposes;
    address private head;
    string private name;
    uint votingTime = 7 days; // duration of the voting on an address
    uint votingThreshold; // used in voting if the voting counter equals or passed the threshold, the voting choice is accepted

    struct voting {
        uint votingEndDate;
        uint Type; // type of the voting : 0 = add member, 1 = remove member, 2 = change head
        uint yesCount; // counter for voting on the address
        mapping (address => bool) voters; // a boolean for addresses to prevent double voting
    }

    mapping(address => bool) private boardMembers;
    address[] boardMembersArray;

    mapping(address => voting) memberVoting; // map connect address with voting
    address[] public memberVotingArray; // array of the voting holding instance for every voting on a person started


    event CreateSubInstit(string message, address _address);
    event CreateSubCourse(string message, address _address);
    event BoardMemberAdded(string message, address _address,uint);
    event BoardMemberRemoved(string message, address _address,uint);
    event ChangeHead(string message, address _address,uint);
    event Vote(string message , address from , address to);
    event VoteChange(string message , address _address);

    modifier onlyHead(){
        require(msg.sender == head);
        _;
    }

    modifier onlyMember(){
        require(boardMembers[msg.sender] == true);
        _;
    }

    modifier headORmember(){
        require(boardMembers[msg.sender] == true || head == msg.sender);
        _;
    }

    function getMemberVotingEndDate(address member) public view returns(uint){
        return memberVoting[member].votingEndDate;
    }

    function getMemberVotingType(address member) public view returns(uint){
        return memberVoting[member].Type;
    }

    function getMemberVotingYesCount(address member) public view returns(uint){
        return memberVoting[member].yesCount;
    }

    function getHead() public view returns(address){
        return head;
    }

    function getCreator() public view returns(address){
        return creator;
    }

    function getName()public view returns(string memory){
        return name;
    }

    function getMemVotingArray()public view returns(address[] memory){
        return memberVotingArray;
    }

    function getVotingThreshold()public view returns(uint){
        return votingThreshold;
    }

    /** @dev Institution Constructor.
      * @param _name Institution name.
      * @param _head Institution head.
      * @param _creator Institution creator, Cant be changed.
      * @param _boardMembers array of board members, has to be more than 2 members.
      */
    constructor(string memory _name , address _head , address _creator , address[] memory _boardMembers) public {
        require(_boardMembers.length >= 2);
        creator = _creator;
        head = _head;
        name = _name;
        votingThreshold = (_boardMembers.length + 1)/2 + 1;
        for(uint i=0 ; i < _boardMembers.length ; i++)
            boardMembers[_boardMembers[i]] = true;
        boardMembersArray = _boardMembers;
    }

    /** @dev Create SubInstitution.
      * @param _name Institution name.
      * @param _head Institution head.
      * @param factory makes the parent Institution as the creator.
      * @param _boardMembers array of board members, has to be more than 2 members.
      * @return the address of the new Institution
      */
    function createSubInstitution(string memory _name , address _head, InstitutionFactory factory , address[] memory _boardMembers )public onlyHead returns(address){
        address institutionAddress = factory.createInstitution(_name , _head , address(this) , _boardMembers);
        emit CreateSubInstit("Created Institution" , institutionAddress);
        return institutionAddress;
    }

    /** @dev Create Course.
      * @param _name Course name.
      * @param _instructors array of instructors.
      * @param _teachingAssistants array of teaching assistants, has to be more than 2 members.
      * @return the address of the course
      */
    function createCourseOffering(string memory _name , address[] memory _instructors , address[] memory _teachingAssistants) public onlyHead returns(address){
        Course newCourse = new Course( _name , _instructors , _teachingAssistants); //deploy course contract and makes the creator the institution address
        emit CreateSubCourse("Created Course",address(newCourse));
        return address(newCourse);
    }

    /** @dev Add Board Member.
      * @param newMember address of the new member.
      */
    function addBoardMembers(address newMember) public onlyHead{
        require(!boardMembers[newMember],"This address is already a board member");
        require(memberVoting[newMember].votingEndDate == 0 , "There is an open voting on this address running");
        checkPassedVoting();

        memberVoting[newMember].votingEndDate = votingTime + now; // voting end date = current time + voting duration
        memberVoting[newMember].yesCount = 1; // equal 1 as it's the head's vote
        memberVoting[newMember].Type = 0;
        memberVoting[newMember].voters[msg.sender] = true;
        memberVotingArray.push(newMember);

        emit BoardMemberAdded("Voting on new board member started..." , newMember , memberVoting[newMember].votingEndDate);
    }

    /** @dev Remove Board Member.
      * @param member address of the member going to be removed.
      */
    function removeBoardMembers(address member) public onlyHead{
        require(boardMembers[member],"This address is already a board member");
        require(memberVoting[member].votingEndDate == 0 , "There is an open voting on this address running");
        checkPassedVoting();

        memberVoting[member].votingEndDate = votingTime + now; // voting end date = current time + voting duration
        memberVoting[member].yesCount = 1; // equal 1 as it's the head's vote
        memberVoting[member].Type = 1;
        memberVoting[member].voters[msg.sender] = true;
        memberVotingArray.push(member);

        emit BoardMemberRemoved("Voting on removing board member started..." , member , memberVoting[member].votingEndDate );
    }

    /** @dev Change Head.
      * @param newHead address of the new head.
      */
    function changeHead(address newHead) public headORmember{
        require(newHead != head , "The address is the same as the head address");
        require(memberVoting[newHead].votingEndDate == 0,"There is an open voting on this address running");
        checkPassedVoting();

        memberVoting[newHead].votingEndDate = votingTime + now; // voting end date = current time + voting duration
        memberVoting[newHead].yesCount = 1; // equal 1 as it's the head's vote
        memberVoting[newHead].Type = 2;
        memberVoting[newHead].voters[msg.sender] = true;
        memberVotingArray.push(newHead);

        emit ChangeHead("Voting on changing current head started..." , newHead , memberVoting[newHead].votingEndDate);
    }

    /** @dev Vote.
      * @param member address of the chosen member.
      */
    function vote(address member) public headORmember {
        require(now <= memberVoting[member].votingEndDate , "The end date already passed");
        require(memberVoting[member].voters[msg.sender] == false , "you already voted on this address");
        memberVoting[member].yesCount++;

        emit Vote("Voted on Adding... " , msg.sender , member);
        // check if the yescount passed the voting Threshold
        // then remove it from the memberVoting array
        // and the member voting map
        if(memberVoting[member].yesCount >= votingThreshold){
            uint i=0;
            uint sz;
            // add member
            if(memberVoting[member].Type == 0){
                // add member to the board member array and map
                boardMembers[member] = true;
                boardMembersArray.push(member);
                sz = memberVotingArray.length;
                for(i=0 ; i < sz ; i++){
                    if(memberVotingArray[i] == member){
                        memberVotingArray[i] = memberVotingArray[sz-1];
                        memberVotingArray.length--;
                        break;
                    }
                }
                // update voting threshold
                votingThreshold = (boardMembersArray.length + 1)/2 + 1;
                // call event to stop voting for this member
                emit VoteChange("This member is Added... " , member);
                // remove member from the map
                delete memberVoting[member];
            }
            //remove member
            else if(memberVoting[member].Type == 1){
                // remove member from the board member array, map and member voting array and map
                boardMembers[member] = false;
                sz = boardMembersArray.length;
                for(i=0 ; i < sz ; i++){
                    if(boardMembersArray[i] == member){
                        boardMembersArray[i] = boardMembersArray[sz-1];
                        boardMembersArray.length--;
                        break;
                    }
                }
                sz = memberVotingArray.length;
                for(i=0 ; i < sz ; i++){
                    if(memberVotingArray[i] == member){
                        memberVotingArray[i] = memberVotingArray[sz-1];
                        memberVotingArray.length--;
                        break;
                    }
                }

                // update voting threshold
                votingThreshold = (boardMembersArray.length + 1)/2 + 1;
                // call event to stop voting for this member
                emit VoteChange("This member is Removed... " , member);
                // remove member from the map
                delete memberVoting[member];
            }
            // change head
            else{
                sz = memberVotingArray.length;
                for(i=0 ; i < sz ; i++){
                    if(memberVotingArray[i] == member){
                        memberVotingArray[i] = memberVotingArray[sz-1];
                        memberVotingArray.length--;
                        break;
                    }
                }
                head = member;
                // remove member from the map
                delete memberVoting[member];
                // call event to stop voting for this member
                emit VoteChange("Head has been changed... " , member);
            }
        }
    }

    /// @dev check finished votings in the array
    function checkPassedVoting() private{
        uint sz = memberVotingArray.length;
        for(uint i=0 ; i < sz ; i++)
        /** Check if the end date of the selected in voting Array
          * then remove it from the member voting array , member
          * voting map and decrease size of the array by 1
          */
            if(now > memberVoting[memberVotingArray[i]].votingEndDate ){
                memberVotingArray[i] = memberVotingArray[sz-1];
                memberVotingArray.length--;
                delete memberVoting[memberVotingArray[i]];
                i--;
            }
    }
}
