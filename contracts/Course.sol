pragma solidity 0.5.0;

/** @title Course. */
contract Course{
    
    // Address of creator institution. 
    address public creator; 
    
    // Represents Assignments, tasks, exams, etc...
    struct GradedItem{
        string name;
        uint maxGrade;
        uint weight; // How much it contributes to coure's final grade.
    }
    
    // If an address maps to true then this address is a member.
    mapping( address => bool ) private instructors;
    mapping( address => bool ) private teachingAssistants;
    
    // Student PK => ( Graded Item ID => Grade ).
    mapping(address => mapping(uint => uint)) private gradedItemGrades;
    
    // Student to course total grade
    mapping(address => uint) private  totalGrades; 
    
    // Id to graded item, id is serial.
    mapping(uint => GradedItem) private gradedItemId;  
    
    string public name;
    uint public startDate;
    uint public endDate;
    
    // Size of graded items map. 
    uint public index = 0;
    
    // Weight to be assigned to graded items, a course is graded out of 100.
    uint public totalWeight = 100;
    
    event UpdateItemGradeDesc(string desc);
    event InstructorsAdded(address[] instructors);
    event InstructorsRemoved(address[] instructors);
    event TeachingAssistantsRemoved(address[] teachingAssistants);
    event TeachingAssistantsAdded(address[] teachingAssistants);
    event StudentsAdded(address[] Students);
    event StudentsRemoved(address[] Students);
    event CourseStartDateEdited(uint time);
    event CourseEndDateEdited(uint time);
    event GradedItemAdded(string name,uint maxgrade, uint weight, uint id);
    event GradedItemUpdated(uint id, uint newWeight);
    event CalculatedGrades(address Student, uint grade);

    modifier onlyInstructor{
        require(instructors[msg.sender] == true);
        _;
    }
    
    modifier onlyTeachingAssistant{
        require(teachingAssistants[msg.sender] == true);
        _;
    }
    
    modifier instructorORteachingAssistant{
        require(teachingAssistants[msg.sender] == true || instructors[msg.sender] == true);
        _;
    }
    
    modifier beforeEndDate{
        require(now < endDate);
        _;
    }
    
    /** @dev Creates a new course, setting a name, instructors and TAs.
      * @param _name Course name.
      * @param _instructors Course's instructors.
      * @param _teachingAssistants Course's TAs.
      */
    constructor(string memory _name , address[] memory _instructors , address[] memory _teachingAssistants)public {
        require(_instructors.length >= 1, "At least one instructor");
        require(_teachingAssistants.length >= 1, "At least one TA");
        uint i;
        for(i=0 ; i<_instructors.length ; i++){
            instructors[_instructors[i]] = true;
        }
        for(i=0 ; i<_teachingAssistants.length ; i++){
            teachingAssistants[_teachingAssistants[i]] = true;
        }
        name = _name;
        creator = msg.sender;
    }
    
    /** @dev Returns size of gradedItemId mapping.
      * @return index.
      */
    function getIndex()public view returns(uint){
        return index;
    }
    
    /** @dev Returns a grade of an item for a specific student.
      * @param _Student Student public key.
      * @param ID Graded item id.
      * @return Item's grade.
      */
    function getGrades(address _Student , uint ID)public view returns(uint){
        return gradedItemGrades[_Student][ID];
    }

    function getItemName(uint ID)public view returns(string memory){
        return (gradedItemId[ID].name);
    }

    function getItemMaxGrade(uint ID)public view returns(uint){
        return (gradedItemId[ID].maxGrade);
    }

    function getItemWeight(uint ID)public view returns(uint){
        return (gradedItemId[ID].weight);
    }
    
    /** @dev Returns course's final grade for a student.
      * @param student Student public key.
      * @return Final grade.
      */
    function getTotalGrade(address student)public view returns(uint){
        return totalGrades[student];
    }
    
    /** @dev Register students to the course.
      * @param _students  array of students' public keys.
      */
    function addStudents(address [] memory _students)public instructorORteachingAssistant beforeEndDate{
        for(uint i=0 ; i<_students.length ; i++){
            totalGrades[_students[i]] = 1;
        }
        emit StudentsAdded(_students);
    }
    
    /** @dev Remove students from the course.
      * @param _students  array of students' public keys.
      */
    function removeStudents(address[] memory _students)public instructorORteachingAssistant beforeEndDate{
        for(uint i=0; i<_students.length ; i++){
            totalGrades[_students[i]] = 0;
        }
        emit StudentsRemoved(_students);
    }
    
    /** @dev Register instructors to the course.
      * @param _instructors  array of instructors' public keys.
      */
    function addInstructor(address [] memory _instructors)public onlyInstructor beforeEndDate{
        for(uint i=0 ; i< _instructors.length ;i++){
            instructors[_instructors[i]] = true;
        }
        emit InstructorsAdded(_instructors);
    }
    
    /** @dev Register teaching assistants to the course.
      * @param _teachingAssistants array of TAs' public keys.
      */
    function addTeachingAssistant(address [] memory _teachingAssistants)public onlyInstructor beforeEndDate{
        for(uint i=0  ;i<_teachingAssistants.length ; i++){
            teachingAssistants[_teachingAssistants[i]] = true;
        }
        emit TeachingAssistantsAdded(_teachingAssistants);
    }
    
    /** @dev Remove instructors from the course.
      * @param _instructors  array of students' public keys.
      */
    function removeInstructor(address[] memory _instructors)public onlyInstructor beforeEndDate{
        for(uint i=0 ; i<_instructors.length ; i++){
            instructors[_instructors[i]] = false;
        }
        emit InstructorsRemoved(_instructors);
    }
    
    /** @dev Register TAs to the course.
      * @param _teachingAssistants  array of TAs' public keys.
      */
    function removeTeachingAssistant(address[] memory _teachingAssistants)public onlyInstructor beforeEndDate{
        for(uint i=0  ;i<_teachingAssistants.length ; i++){
            teachingAssistants[_teachingAssistants[i]] = false;
        }
        emit TeachingAssistantsRemoved(_teachingAssistants);
    }
    
    /** @dev Add graded item to the course.
      * @param _name Item's name.
      * @param _maxGrade Item's max grade.
      * @param _weight Item's weight.
      */
    function addGradedItem(string memory _name,uint _maxGrade, uint _weight)public onlyInstructor beforeEndDate{
        require(totalWeight >= _weight);
        
        totalWeight -= _weight;
        gradedItemId[index] = GradedItem(_name, _maxGrade, _weight);
        emit GradedItemAdded(_name, _maxGrade, _weight, index);
        index++;
    }
        
    /** @dev Update graded item weight to coure's total grade.
      * @param _id Graded item id.
      * @param _weight New weight to be assigned.
      */
    function updateGradedItemWeight(uint _id, uint _weight)public onlyInstructor beforeEndDate{
        require(_id >= 0 && _id < index );
        require((totalWeight + gradedItemId[_id].weight ) >= _weight);
        
        totalWeight += gradedItemId[_id].weight;
        totalWeight -= _weight;
        gradedItemId[_id].weight = _weight;
        emit GradedItemUpdated(_id, _weight);
    }
    
    /** @dev Update graded item grade for a student.
      * @param _student student's public keys.
      * @param _grade student's grade.
      * @param _id Graded item id.
      * @param desc Grading description 
      */
    function updateItemGrade(address _student, uint _grade, uint _id, string memory desc)public instructorORteachingAssistant{
        require( totalGrades[_student] != 0);
        require( now > startDate && startDate != 0);
        require(_id >= 0 && _id < index );
        require( _grade <= gradedItemId[_id].maxGrade);
        
        gradedItemGrades[_student][_id] = _grade;
        if(now > endDate ){
            // If an item graded is updated after the course end date, student's total grade is recalculated.
            calculateTotalGrades(_student);
        }
        emit UpdateItemGradeDesc(desc);
    }
    
    /** @dev Calculates student total grade based on graded items grades.
      * @param student student's public keys.
      */
    function calculateTotalGrades (address student)public {
        require(totalWeight == 0);
        require( now > endDate && endDate != 0);
        require( totalGrades[student] != 0 );
        
        totalGrades[student] = 0;
        for(uint id = 0 ; id < index ; id++){
            // Adding the grade of each graded item to the student's total grade.
            // Grade of each item is multiplied by item's weight then divided by the item's max grade.
            if(gradedItemGrades[student][id] != 0)
                totalGrades[student] +=Ceil( (gradedItemId[id].weight * gradedItemGrades[student][id]), (gradedItemId[id].maxGrade) );
        }

        emit CalculatedGrades(student, totalGrades[student]);
    }

    function Ceil(uint x , uint y)private pure returns(uint){
        return (x+y-1)/y;
    }
    
    /** @dev Sets course start.
      * @param time Time in seconds.
      */
    function courseStartDate(uint time)public onlyInstructor{
        require(startDate == 0 || now < startDate);
        require( now < time);
        require( endDate == 0  ||time < endDate);
        
        startDate = time;
        emit CourseStartDateEdited(time);
    }
    
    /** @dev Sets course end date.
      * @param time Time in seconds.
      */
    function courseEndDate(uint time)public onlyInstructor{
        require(startDate < time && startDate!=0);
        require(now < time);
        
        endDate = time;
        emit CourseEndDateEdited(time);
    }
    
}
