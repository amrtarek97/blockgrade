App = { //app is equal to big object
    web3Provider: null,
    contracts: {},

    init: function () {
        console.log("App Initialized...");
        return App.initWeb3();
    },

    initWeb3: function () {
        // Is there an injected web3 instance?
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
        } else {
            // If no injected web3 instance is detected, fall back to Ganache
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
        }
        web3 = new Web3(App.web3Provider);

        return App.initContract(); //App.initContracts also works.
    },

    initContract: function () {
        $.getJSON('InstitutionFactory.json', function (data) {
            // Get the necessary contract artifact file and instantiate it with truffle-contract
            App.contracts.InstitutionFactory = TruffleContract(data);

            // Set the provider for our contract
            App.contracts.InstitutionFactory.setProvider(App.web3Provider);

            App.contracts.InstitutionFactory.deployed().then(function (instance) {
                console.log("InstitutionFactory address : ", instance.address);
            });
        });

        return App.bindEvents();
    },

    bindEvents: function () {
        $(document).on('click', '.btn-changePage', App.changePage);
        $(document).on('click', '.btn-backtrack', App.backtrackPage);
        $(document).on('click', '.btn-backtrack2', App.backtrackPage2);

        $(document).on('click', '.btn-createInst', App.createInstitution);
        $(document).on('click', '.btn-subInst', App.addSubInstitution);
        $(document).on('click', '.btn-subCor', App.addSubCourse);
        $(document).on('click', '.btn-addMem', App.addBoardMember);
        $(document).on('click', '.btn-rmvMem', App.rmvBoardMember);
        $(document).on('click', '.btn-changeHead', App.changeHead);
        $(document).on('click', '.btn-votePg', App.printVoting); // for printing the content in votelist array
        $(document).on('click', '.btn-vote', App.vote);

        $(document).on('click', '#GoCourse-btn', App.initCoursePg);
        $(document).on('click', '.add_instructor', App.AddInstructor);
        $(document).on('click', '.remove_instructor', App.RemoveInstructor);
        $(document).on('click', '.CourseDuration', App.CourseDuration);
        $(document).on('click', '.CourseStartDate', App.ChangeStartDate);
        $(document).on('click', '.remove_TA', App.RemoveTA);
        $(document).on('click', '.add_TA', App.AddTA);
        $(document).on('click', '.AddGradedItem-btn', App.AddGradedItem);
        $(document).on('click', '.UpdateGradedItem-btn', App.UpdateItemWeight);
        $(document).on('click', '.UpdateStudentGrade-btn', App.UpdateStudentGrade);
        $(document).on('click', '.ComputeStudentGrade-btn', App.ComputeGrade);
        $(document).on('click', '.add-student-btn', App.AddStudent);
        $(document).on('click', '.RemStudent-btn', App.RemoveStudent);
        $(document).on('click', '#Student-btn', App.getGrades);
    },

    createInstitution: function(event){
        event.preventDefault();
        $("#loader").show();
        var headPK = document.getElementById("Head-PK").value;
        var InstName = document.getElementById("InstitutionName").value;
        var creatorPK = document.getElementById("Creator-PK").value;
        var boardMemPK = [document.getElementById("BMember-Key1").value, document.getElementById("BMember-Key2").value];

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            App.contracts.InstitutionFactory.deployed().then(function (instance) {
                // Execute createIns as a transaction by sending account
                instance.createInstitution(InstName, headPK,creatorPK, boardMemPK, {from: account});
                instance.CreateInstitution().watch(function (err, res) {
                    if (!err) {
                        $("#loader").hide();
                        document.getElementById("Institution-Address").innerHTML = (res.args)._address;
                        //console.log(res);
                    }
                });
            }).catch(function (err) {
                console.log(err.message);
            });
            
        });
    },

    changePage: function(){
        var Institution_Address = document.getElementById("InstAddress").value;
        localStorage.setItem('InstitutionAddress', Institution_Address);
    },

    backtrackPage: function(){
        App.contracts.Institution.getCreator(function (err,creator) {
            if(!err)
                localStorage.setItem('InstitutionAddress', creator);
        });
    },

    backtrackPage2: function(){
        App.contracts.CourseInst.creator(function (err,creator) {
            if(!err)
                localStorage.setItem('InstitutionAddress', creator);
        });
    },

    initInstPg: function () {
        var Inst_Address = localStorage.getItem('InstitutionAddress');
        $.getJSON('Institution.json', function (data) {
            // Get the necessary contract artifact file and instantiate it with truffle-contract
            var instance = web3.eth.contract(data.abi).at(Inst_Address);
            //var instance = App.contracts.Institution;
            App.contracts.Institution = instance;

            console.log("Institution SC address:", instance.address); //inspect element to see the console logs.
            instance.getName(function (err,value) {
                document.getElementById("Institution-Name").innerHTML = value;
            });
        });
    },

    addSubInstitution: function (event) {
        event.preventDefault();
        $("#loader").show();
        var headPK = document.getElementById("Head-Public-key1").value;
        var InstName = document.getElementById("Inst-Name").value;
        var boardMemPK = [document.getElementById("Board-Member-Key1").value, document.getElementById("Board-Member-Key2").value];
        var factoryAddress;

        App.contracts.InstitutionFactory.deployed().then(function (instance) {
            factoryAddress = instance.address;
        });

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            // Execute createSubIns as a transaction by sending account
            App.contracts.Institution.createSubInstitution(InstName, headPK, factoryAddress, boardMemPK, {from: account},function (err,res) {
                if(err){
                    console.log(err.message);
                }
            });
            App.contracts.Institution.CreateSubInstit().watch(function (err, res) {
                if (!err) {
                    $("#loader").hide();
                    document.getElementById("SubInstitution-Address").innerHTML = (res.args)._address;
                }
            });
        });
    },

    addSubCourse: function (event) {
        event.preventDefault();
        $("#loader1").show();
        var courseName = String(document.getElementById("Course-Name").value);
        var Instructor_PK = [document.getElementById("Instructor-Public-key").value];
        var TA_PKs = [document.getElementById("TA-Key1").value, document.getElementById("TA-Key2").value];

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            // Execute createSubIns as a transaction by sending account
            App.contracts.Institution.createCourseOffering(courseName, Instructor_PK, TA_PKs, {from: account},function (err , res) {
                if(err){
                    console.log(err.message);
                }
            });

            App.contracts.Institution.CreateSubCourse().watch(function (err, res) {
                if (!err) {
                    $("#loader1").hide();
                    $("#CourseAddress-Output").html("Course Address:  " + (res.args)._address);
                }
            });

        });
    },

    addBoardMember: function (event) {
        event.preventDefault();

        var boardMemPK = document.getElementById("add-Member-Public-key").value;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            App.contracts.Institution.addBoardMembers(boardMemPK, {from: account},function(err,res){
                if(!err){
                    alert("New Member added to Voting page");
                }
            });
        });
    },

    rmvBoardMember: function (event) {
        event.preventDefault();

        var boardMemPK = document.getElementById("rmv-Member-Public-key").value;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            App.contracts.Institution.removeBoardMembers(boardMemPK, {from: account},function(err,res){
                if(!err){
                    alert("New Member added to Voting page");
                }
            });
        });
    },

    changeHead: function (event) {
        event.preventDefault();

        var newHeadPK = document.getElementById("NewHead-Public-key").value;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            App.contracts.Institution.changeHead(newHeadPK, {from: account},function(err,res){
                if(!err){
                    alert("New Member added to Voting page");
                }
            });
        });
    },

    printVoting: function () {
        var instance = App.contracts.Institution;
        instance.getMemVotingArray(function (err,array) {
            instance.getVotingThreshold(function (err,threshold) {
                $(document).ready(function () {
                    $("#printVoting").html("Voting Threshold : " + threshold + "<br>");
                    $.each(array, function (i, val) {
                        instance.getMemberVotingEndDate(val,function (err,endDate) {
                            instance.getMemberVotingType(val,function (err,Type) {
                                instance.getMemberVotingYesCount(val,function (err,yesCount) {
                                    $("#printVoting").append(i + ": " + val + " " + endDate + " " + Type + " " + yesCount + "<br>");
                                });
                            });
                        });
                    });
                });
            });
        });
    },

    vote: function (event) {
        event.preventDefault();
        var member = document.getElementById("votersPK").value;

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            App.contracts.Institution.vote(member, {from: account},function (err , res) {
                if(!err){
                    alert("Voted !!");
                }
            });
        });
    },

    initCoursePg: function (event) {
        //event.preventDefault();
        var Course_Address = localStorage.getItem('CourseAddress');
        console.log("Course Addess: ", Course_Address);
        var Start_date;
        var End_date;

        $.getJSON('Course.json', function (Course_data) {
            console.log(Course_data);
            var Course_instance = web3.eth.contract(Course_data.abi).at(Course_Address);
            App.contracts.CourseInst = Course_instance;
            console.log(App.contracts.CourseInst);

            Course_instance.name(function (err, name) {
                $("#course_name").append(name);
            });
            Course_instance.startDate(function (err, startDate) {
                Start_date = new Date(startDate.toNumber() * 1000);
                $("#StartDate").append(Start_date);
            });
            Course_instance.endDate(function (err, endDate) {
                End_date = new Date(endDate.toNumber() * 1000);
                $("#EndDate").append(End_date);
            })

        });

        return App.bindEvents();

    },


    CourseDuration: function (event) {

        event.preventDefault();

        var year = document.getElementById("year").value;
        var month = document.getElementById("month").value;
        var day = document.getElementById("day").value;
        var date = new Date(year + "-" + month + "-" + day);

        web3.eth.getAccounts(function (error, accounts) {

            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            var seconds = date.getTime() / 1000;

            var event = App.contracts.CourseInst.CourseEndDateEdited({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
            });

            console.log("Adding Instructors Addresses");
            App.contracts.CourseInst.courseEndDate(seconds, {from: account}, function (err, res) {
                if (!err) {
                    console.log(res);
                    $("#endDate_changed").append("Course end date updated Successfuly");
                }
                else {
                    console.log(err);
                }

            });
        });

    },


    ChangeStartDate: function (event) {

        event.preventDefault();

        var year = document.getElementById("_year").value;
        var month = document.getElementById("_month").value;
        var day = document.getElementById("_day").value;
        var date = new Date(year + "-" + month + "-" + day);
        web3.eth.getAccounts(function (error, accounts) {

            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var seconds = date.getTime() / 1000;
            var event = App.contracts.CourseInst.CourseStartDateEdited({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
            });

            App.contracts.CourseInst.courseStartDate(seconds, {from: account}, function (err, res) {
                if (!err) {
                    console.log(res);
                    $("#startdate_changed").append("Course Started Successfuly");
                }
                else {
                    console.log(err);
                }

            });

        });

    },


    AddInstructor: function (event) {

        event.preventDefault();
        var Instructors = [document.getElementById("inst1").value, document.getElementById("inst2").value];


        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.InstructorsAdded({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
            });

            console.log("Adding Instructors Addresses");
            App.contracts.CourseInst.addInstructor(Instructors, {from: account}, function (err, res) {
                if (!err) {
                    console.log(res);
                    $("#Addinst-Output").append("Successfully Added!");
                }
                else {
                    console.log(err);
                }
            });

        });


    },

    RemoveInstructor: function (event) {

        event.preventDefault();
        var Instructor = [];
        Instructor.push(document.getElementById("inst_to_be_removed").value);


        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.InstructorsRemoved({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
                else {
                    console.log(err);
                }
            });

            console.log("Instructor address to be removed : ", Instructor);
            App.contracts.CourseInst.removeInstructor(Instructor, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                    $("#Removeinst-Output").append("Successfuly Removed!");
                }
                else {
                    console.log(err);
                }

            });
        });

    },


    AddTA: function (event) { //have to add at least 2TAs on the submit

        event.preventDefault();
        console.log(document.getElementById("Add-TA-PK1").value);
        var TA_key = [document.getElementById("Add-TA-PK2").value, document.getElementById("Add-TA-PK2").value];

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.TeachingAssistantsAdded({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
                else {
                    console.log(err);
                }
            });

            console.log("TA addresses to be added");
            App.contracts.CourseInst.addTeachingAssistant(TA_key, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                    $("#Add-Output").append("Added!");
                }

                else {
                    console.log(err);
                }

            });

        });
    },


    RemoveTA: function (event) {
        event.preventDefault();
        var TA_key = [];
        TA_key.push(document.getElementById("Rem-TA-PK").value);
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.TeachingAssistantsRemoved({}, {
                fromBlock: 'latest',
                toBlock: 'latest'
            });

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
            });

            console.log("TA address to be removed : ", TA_key);
            App.contracts.CourseInst.removeTeachingAssistant(TA_key, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                    $("#Rem-Output").append("TA Removed!");
                }

            });
        });


    },

    AddStudent: function (event) {

        event.preventDefault();
        var StudentPK = [];
        StudentPK.push(document.getElementById("AddstudentPK").value);

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.StudentsAdded({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
            });

            App.contracts.CourseInst.addStudents(StudentPK, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                    $("#AddStudent-Output").append("Students Added!");
                }

            });
        });

    },

    RemoveStudent: function (event) {
        event.preventDefault();
        var StudentPK = [];
        StudentPK.push(document.getElementById("RemStudentPK").value);

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.StudentsRemoved({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
            });

            App.contracts.CourseInst.removeStudents(StudentPK, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                    $("#RemStudent-Output").append("Removed!");
                }

            });
        });
    },

    AddGradedItem: function (event) {
        event.preventDefault();
        var ItemName = String(document.getElementById("ItemName").value);
        var MaxGrade = parseInt(document.getElementById("ItemMaxGrade").value);
        var ItemWeight = parseInt(document.getElementById("ItemWeight").value);
        console.log(MaxGrade + "  " + ItemWeight);
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            console.log("Item name : ", ItemName);

            var event = App.contracts.CourseInst.GradedItemAdded({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                }
                else {
                    console.log("Event Error: ", err);
                }
            });

            App.contracts.CourseInst.addGradedItem(ItemName, MaxGrade, ItemWeight, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                    App.contracts.CourseInst.getIndex( function(err,res){
                        $("#ItemAdded-Output").html(" Succefully Added!");
                        $("#ItemAdded-Output").append(" Graded item ID is ");
                        $("#ItemAdded-Output").append(res-1);
                    })
                }

            });
        });
    },

    UpdateItemWeight: function (event) {

        event.preventDefault();
        var ItemId = parseInt(document.getElementById("ItemId").value);
        var Weight = parseInt(document.getElementById("ItemNewWeight").value);

        console.log(ItemId);
        console.log(Weight);
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            var event = App.contracts.CourseInst.GradedItemUpdated({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);

                }
            });
            App.contracts.CourseInst.updateGradedItemWeight(ItemId, Weight, {from: account}, function (err, res) {

                if (!err) {
                    console.log((res.args).newWeight.to_Number);
                    $("#ItemWeightUpdate-Output").append("Item Weight Updated!");
                }

            });
        });

    },

    UpdateStudentGrade: function (event) {
        event.preventDefault();

        var StudentPK=String(document.getElementById("StudentPK").value);
        var Grade = parseInt(document.getElementById("StudentGrade").value);
        var ItemId = parseInt(document.getElementById("StudentItemId").value);
        var desc = String(document.getElementById("Desc").value);


        console.log("Student Address:", StudentPK, " " , Grade, " ", ItemId, " " , desc);
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

        var evnt = App.contracts.CourseInst.UpdateItemGradeDesc({}, {fromBlock: 'latest', toBlock: 'latest'});

        evnt.watch(function (err, Event) {
            if (!err) {
                console.log(Event);
                console.log("Block Number: ", Event.blockNumber);
            }
        });
        App.contracts.CourseInst.name(function (err, name) {
                console.log(name);
           });
        App.contracts.CourseInst.updateItemGrade(StudentPK, Grade, ItemId, desc, {from: account}, function (err, res) {

            if (!err) {
                console.log(res);
                $("#StudentGrade-Output").append("Student Grade Updated!");
            }
		});
    });


    },

    ComputeGrade: function (event) {

        event.preventDefault();

        var Student_address = String(document.getElementById("student_address").value);

        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];

            var event = App.contracts.CourseInst.CalculatedGrades({}, {fromBlock: 'latest', toBlock: 'latest'});

            event.watch(function (err, Event) {
                if (!err) {
                    console.log(Event);
                    console.log("Block Number: ", Event.blockNumber);
                    $("#ComputeStudent_output").append("Total grade: " +  Event.args.grade);
                }
            });

            console.log("Student Address:", Student_address);

            App.contracts.CourseInst.calculateTotalGrades(Student_address, {from: account}, function (err, res) {

                if (!err) {
                    console.log(res);
                }

            });
        });
    },

    getGrades:  function(event){
        event.preventDefault();

        var Course_Address = document.getElementById("CourseAddress").value;
        var Student_Address = String(document.getElementById("StudentAddress").value);

        $.getJSON('Course.json',  function (Course_data) {
            var Course_instance = web3.eth.contract(Course_data.abi).at(Course_Address);

            web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            Course_instance.name(function (err, name) {
                console.log(name);
            });
            Course_instance.getTotalGrade(Student_Address, {from: account}, function (err, grade) {
                $("#PrintGrades").html("Final grade is: "+grade + " out of 100");
                $("#PrintGrades").append("Your course information :" + "<br>");
                Course_instance.getIndex(function(err,index){
                	var indexArray= [];
                	for(var j= 0 ; j< index ; j++){
                		indexArray[j] = 0;
                	}
                	$.each(indexArray, function(i){
                		Course_instance.getGrades(Student_Address,i, function(err,grade){
                			Course_instance.getItemName(i, function(err,itemName){
                				 Course_instance.getItemMaxGrade(i, function(err,itemMaxGrade){
                				 	 Course_instance.getItemWeight(i, function(err, itemWeight){
                				 	 	$("#PrintGrades").append(i + " => " + itemName + " " + itemMaxGrade + " " + itemWeight + " " + grade + "<br>");
                				 	 });
                				 });
                			});
                		});
                	});
                })
            });


            });

        });
    }

};


//what this means is whenever the server loads the window we want to initialize our app (App.initWeb3).
$(document).ready(function () {
   if (window.location.pathname === "/Course.html") {

        $(function () {
            App.initCoursePg();
            console.log("App Course..");
        });

    }
    else {
        $(function () {
            App.initWeb3();
            console.log("App Institution..");
        });
    }

});

